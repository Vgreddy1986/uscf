package com.qa.vg.base;

import java.io.File;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Parameters;

import com.qa.uscf.util.Log;
import com.qa.vg.utils.ReadYamlConfig;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * @Author vijaya karne
 */
public class BasePage {
	public static WebDriver driver;
	public static String browserName;
	public static String screenshotName;
   public static ThreadLocal<WebDriver> tlDriver=new ThreadLocal<WebDriver>();
   
   
   public static synchronized WebDriver getDriver() {
		return tlDriver.get();
   }
   /**
	 * This method is used to initialize the WebDriver on the basis of
	 * browserName
	 * 
	 * @param browserName
	 * @return this method will return driver instance
	 */
	
	public WebDriver init_driver(String browser) {
		
		System.out.println("browser given" +browser);
		if(browser.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			tlDriver.set(new  ChromeDriver());
			getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			System.out.println("Launching Chrome browser");
			
			
			
			
			  }else if(browser.equalsIgnoreCase("firefox")){
			  WebDriverManager.firefoxdriver().setup(); tlDriver.set(new FirefoxDriver());
			  System.out.println("Launching firefox browser");
			  
			  }else if(browser.equalsIgnoreCase("edge")) {
			  
			  WebDriverManager.edgedriver().setup(); tlDriver.set(new EdgeDriver());
			  getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  
			  System.out.println("Launching Edge browser");
			 
		}else {
			System.out.println("browser not found-->  "+browser);
		}
		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
		getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
		return getDriver();
		}
	
		
	
	
			
	

	public String getScreenShotPath(String testCaseName)throws IOException {
		File source =((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
		Date d = new Date();
		screenshotName = testCaseName+"_"+d.toString().replace(":", "_").replace(" ", "_") + ".jpg";
		String destinationFile = System.getProperty("user.dir")+"/Reports/"+screenshotName;
		FileUtils.copyFile(source,new File(destinationFile));
		return destinationFile;
	}
}

