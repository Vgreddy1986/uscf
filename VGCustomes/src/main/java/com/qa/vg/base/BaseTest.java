package com.qa.vg.base;


import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.qa.vg.pages.HomePage;
import com.qa.vg.pages.LoginPage;
import com.qa.vg.utils.ReadYamlConfig;

public class BaseTest {
	public BasePage basepage;
	public LoginPage loginpage;
	public HomePage homepage;
	public Properties prop;
	public WebDriver driver;
	
	@BeforeTest
	@Parameters({"browser"})
	public void setup(String browserName) {
		basepage=new BasePage();
		//String browser=ReadYamlConfig.getbrowser();
		driver=basepage.init_driver(browserName);
		loginpage= new LoginPage(driver);
		
		driver.get(ReadYamlConfig.geturl());
		
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
