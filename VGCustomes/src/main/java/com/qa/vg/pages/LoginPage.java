package com.qa.vg.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.qa.vg.base.BasePage;
import com.qa.vg.utils.Constants;
import com.qa.vg.utils.ElementUtil;

public class LoginPage extends BasePage {
	WebDriver driver;
	 ElementUtil elementutil;
	
	/**	 *1. By locators for login page
	 */
    By id=By.id("Username");
	By password= By.id("Password");
	By login=By.xpath("//button[@value='login']");
	
	By alerterror=By.xpath("//div[@class='validation-summary-errors']");
		
	/**
	 *2. creating constructor
	 * @param driver
	 */
	public LoginPage(WebDriver driver) {
		this.driver=driver;
		elementutil=new ElementUtil(this.driver);
	}
/**
 * login action
 * @param adminUser
 * @param adminPswd
 * @throws InterruptedException 
 */
	
	public HomePage doLogin(String user, String pswd) throws InterruptedException {
	driver.findElement(id).clear();
	Thread.sleep(3000);
	System.out.println("System is ready to accept user name" +user);
	
	elementutil.doSendKeys(id, user);
	elementutil.doSendKeys(password, pswd);
	elementutil.doClick(login);
	return new HomePage(driver);
		
		}
	public String getPageTitle() {
	
		return elementutil.doGetPageTitle();
	}
	public String getCurrentUrl(){
		return elementutil.doGetCurrentUrl();
}
	
	public String geterrormessage(){
		return elementutil.doGetText(alerterror);
	}
	public String getPasswordErrorText() {
		return elementutil.doGetText(alerterror);
	}
	public String getEmailErrorText() {
		return elementutil.doGetText(alerterror);
	}
	public String getCurrentURL() {
		return elementutil.doGetCurrentUrl();
		
	}
	
	

	
}
