package com.qa.vg.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.qa.uscf.util.Log;
import com.qa.vg.base.BaseTest;
import com.qa.vg.pages.LoginPage;
import com.qa.vg.utils.Constants;
import com.qa.vg.utils.ReadYamlConfig;

public class LoginPageTest extends BaseTest{
	WebDriver driver;
	//LoginPage login=new LoginPage(driver);
	
 @Test(priority=1)
	  public void verifyPageTitle() { 
		  String title=loginpage.getPageTitle(); 
		  Assert.assertEquals(title,Constants.pageTitle);
		  
	  }
	 
	

@Test(priority=2)
public void doLoginWithEmptyPassword() throws InterruptedException {

 loginpage.doLogin(ReadYamlConfig.getUser(), " ");
 Log.info(loginpage.getPasswordErrorText());
 Assert.assertEquals("The Password field is required.",loginpage.getPasswordErrorText());
 Log.pass(this.getClass().getName()+" : Passed Successfully");
 
}
@Test(priority=3)
public void doLoginWithEmptyEmail() throws InterruptedException {

 loginpage.doLogin("", ReadYamlConfig.getPassword());
 Log.info(loginpage.getEmailErrorText());
 Assert.assertEquals("The Username field is required.",loginpage.getEmailErrorText());
 Log.pass(this.getClass().getName()+" : Passed Successfully");
 
}
@Test(priority=4)
public void doLoginWithErrorPassword() throws InterruptedException {

 loginpage.doLogin("", "");
 Log.info(loginpage.geterrormessage());
 Assert.assertEquals("The Username field is required.\r\n"
 		+ "The Password field is required.",loginpage.geterrormessage());
 Log.pass(this.getClass().getName()+" : Passed Successfully");
 
}
@Test(priority=5)
public void doLogin() throws InterruptedException {

 loginpage.doLogin(ReadYamlConfig.getUser(), ReadYamlConfig.getPassword());
 Assert.assertEquals(loginpage.getCurrentURL(),ReadYamlConfig.getCurrentUrl());
	
}














}

 